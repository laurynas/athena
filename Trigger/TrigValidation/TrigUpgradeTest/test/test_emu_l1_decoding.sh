#!/bin/sh

# This is not an ART test, but a unit test

# This test should be a unit test in the L1Decoder package

athena.py --imf --threads=1 TrigUpgradeTest/EmuL1DecodingTest.py
